const formatMessage = Scratch.formatMessage

export const intlMapGen = () => ({
  Actuator_geekservomap: formatMessage({ id: "Actuator_geekservomap", default: "GeekServo Map [DEG] ° "}),
  Actuator_geekservo2kgmap: formatMessage({ id: "Actuator_geekservo2kgmap", default: "GeekServo 2KG Map [DEG] ° "}),
  Actuator_servoSetup: formatMessage({ id: "Actuator_servoSetup", default: "intlMap"}),
  Actuator_servoWrite: formatMessage({ id: "Actuator_servoWrite", default: "Servo Write [PIN] Degree[DEG]"}),
  Actuator_buzzer: formatMessage({ id: "Actuator_buzzer", default: "Buzzer Pin [PIN] Freq [FREQ] Delay [DELAY]"}),
  Actuator_music: formatMessage({ id: "Actuator_music", default: "Music Pin [PIN] Notes [NOTES]"}),
  Actuator_relay: formatMessage({ id: "Actuator_relay", default: "Relay Pin [PIN] [ONOFF]"}),
  Actuator_motorModule: formatMessage({ id: "Actuator_motorModule", default: "Motor [MOTOR] DIR[DIR] PWM[PWM] SPEED [SPEED]"}),
  Actuator_motorH: formatMessage({ id: "Actuator_motorH", default: "Motor [MOTOR] IN1[IN1] IN2[IN2] SPEED [SPEED]"}),
  Actuator_menu_onoff_HIGH: formatMessage({ id: "Actuator_menu_onoff_HIGH", default: "HIGH" }),
  Actuator_menu_onoff_LOW: formatMessage({ id: "Actuator_menu_onoff_LOW", default: "LOW" })
})