/**
 * Created by Riven on 2017/12/13.
 */

import { intlMapGen } from './intlMap'

const ArgumentType = Scratch.ArgumentType;
const BlockType = Scratch.BlockType;
const formatMessage = Scratch.formatMessage;
const log = Scratch.log;

class ActuatorExtension{
    constructor (runtime){
        this.EXTENSION_ID = 'Actuator';
        this.runtime = runtime;
    }

    _buildMenuFromArray (ary){
        return ary.map((entry, index) => {
            const obj = {};
            obj.text = entry;
            obj.value = String(entry);
            return obj;
        });
    }

    getInfo (){
        const intlMap = intlMapGen()
        return {
            id: 'Actuator',
            name: formatMessage({
                id: 'Actuator.categoryName',
                default: 'Actuator'
            }),
            color1: '#40BF4A',
            color2: '#2E8934',
            color3: '#2E8934',

            blocks: [
                {
                    opcode: 'servoSetup',
                    blockType: BlockType.COMMAND,
                    text: intlMap.Actuator_servoSetup,
                    arguments: {
                        PIN: {
                            type: ArgumentType.STRING,
                            defaultValue: '4',
                            menu: 'digiPin'
                        }
                    },
                    func: 'servoSetup',
                    gen: {
                        arduino: this.servoSetupGen
                    }
                },
                {
                    opcode: 'servoWrite',
                    blockType: BlockType.COMMAND,
                    text: intlMap.Actuator_servoWrite,
                    arguments: {
                        PIN: {
                            type: ArgumentType.STRING,
                            defaultValue: '4',
                            menu: 'digiPin'
                        },
                        DEG: {
                            type: ArgumentType.NUMBER,
                            defaultValue: 90
                        }
                    },
                    func: 'servoWrite',
                    gen: {
                        arduino: this.servoWriteGen
                    }
                },
                {
                    opcode: 'geekservomap',
                    blockType: BlockType.REPORTER,
                    text: intlMap.Actuator_geekservomap,
                    arguments: {
                        DEG: {
                            type: ArgumentType.NUMBER,
                            defaultValue: 90
                        }
                    },
                    func: 'geekservomap',
                    gen: {
                        arduino: this.geekservomapGen
                    }
                },
                {
                    opcode: 'geekservo2kgmap',
                    blockType: BlockType.REPORTER,
                    text: intlMap.Actuator_geekservo2kgmap,
                    arguments: {
                        DEG: {
                            type: ArgumentType.NUMBER,
                            defaultValue: 90
                        }
                    },
                    func: 'geekservo2kgmap',
                    gen: {
                        arduino: this.geekservo2kgmapGen
                    }
                },
                '---',
                {
                    opcode: 'buzzer',
                    blockType: BlockType.COMMAND,
                    text: intlMap.Actuator_buzzer,
                    arguments: {
                        PIN: {
                            type: ArgumentType.STRING,
                            defaultValue: '5',
                            menu: 'digiPin'
                        },
                        FREQ: {
                            type: ArgumentType.NUMBER,
                            defaultValue: 300
                        },
                        DELAY: {
                            type: ArgumentType.NUMBER,
                            defaultValue: 500
                        }
                    },
                    func: 'buzzer',
                    gen: {
                        arduino: this.buzzerGen
                    }
                },
                {
                    opcode: 'music',
                    blockType: BlockType.COMMAND,
                    text: intlMap.Actuator_music,
                    arguments: {
                        PIN: {
                            type: ArgumentType.STRING,
                            defaultValue: '5',
                            menu: 'digiPin'
                        },
                        NOTES: {
                            type: ArgumentType.STRING,
                            defaultValue: 'g5:1 d c g4:2 b:1 c5:3 '
                        }
                    },
                    func: 'music',
                    gen: {
                        arduino: this.musicGen
                    }
                },
                '---',
                {
                    opcode: 'relay',
                    blockType: BlockType.COMMAND,
                    text: intlMap.Actuator_relay,
                    arguments: {
                        PIN: {
                            type: ArgumentType.STRING,
                            defaultValue: '5',
                            menu: 'digiPin'
                        },
                        ONOFF: {
                            type: ArgumentType.STRING,
                            defaultValue: 'HIGH',
                            menu: 'onoff'
                        }
                    },
                    func: 'relay',
                    gen: {
                        arduino: this.relayGen
                    }
                },
                {
                    opcode: 'motorModule',
                    blockType: BlockType.COMMAND,
                    text: intlMap.Actuator_motorModule,
                    arguments: {
                        MOTOR: {
                            type: ArgumentType.STRING,
                            defaultValue: 'L298N',
                            menu: 'motorList'
                        },
                        DIR: {
                            type: ArgumentType.STRING,
                            defaultValue: '3',
                            menu: 'digiPin'
                        },
                        PWM: {
                            type: ArgumentType.STRING,
                            defaultValue: '5',
                            menu: 'analogWritePin'
                        },
                        SPEED: {
                            type: ArgumentType.SLIDER,
                            defaultValue: 100
                        }
                    },
                    func: 'motor',
                    gen: {
                        arduino: this.motorGen
                    }
                },
                {
                    opcode: 'motorH',
                    blockType: BlockType.COMMAND,
                    text: intlMap.Actuator_motorH,
                    hideFromPalette: true,
                    arguments: {
                        MOTOR: {
                            type: ArgumentType.STRING,
                            defaultValue: 'DRV8833',
                            menu: 'motorHBridge'
                        },
                        IN1: {
                            type: ArgumentType.STRING,
                            defaultValue: '5',
                            menu: 'analogWritePin'
                        },
                        IN2: {
                            type: ArgumentType.STRING,
                            defaultValue: '6',
                            menu: 'analogWritePin'
                        },
                        SPEED: {
                            type: ArgumentType.SLIDER,
                            defaultValue: 100
                        }
                    },
                    func: 'noop',
                    gen: {
                        arduino: this.motorHGen
                    }
                }
            ],
            menus: {
                digiPin: this._buildMenuFromArray(['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13',
                    'A0', 'A1', 'A2', 'A3', 'A4', 'A5']),
                analogWritePin: this._buildMenuFromArray(['3', '5', '6', '9', '10', '11']),
                onoff: [{text: 'ON', value: 'HIGH'}, {text: 'OFF', value: 'LOW'}],
                '#actuatorCatalog':  {
                    acceptReporters: true,
                    items:[
                    {src: 'static/extension-assets/arduino/Servo.png',
                        value: 'Servo', width: 128, height: 128, alt: 'Servo'},
                    {src: 'static/extension-assets/arduino/Buzzer.png',
                        value: 'Buzzer', width: 128, height: 128, alt: 'Buzzer'},
                    {src: 'static/extension-assets/arduino/Motor.png',
                        value: 'Motor', width: 128, height: 128, alt: 'Motor'},
                    {src: 'static/extension-assets/arduino/Relay.png',
                        value: 'Relay', width: 128, height: 128, alt: 'Relay'},
                    {src: 'static/extension-assets/arduino/Stepper.png',
                        value: 'Stepper', width: 128, height: 128, alt: 'Stepper'}
                ]},
                motorList: ['L298N', 'L9110', 'DRV8833', 'H-Bridge'],
                motorHBridge: ['DRV8833', 'H-Bridge']
            },
            _translation_map: {
                'zh-cn': {
                    geekservomap: "GeekServo 映射 [DEG] °",
                    geekservo2kgmap: "GeekServo 2KG 映射 [DEG] °",
                    onoff: {
                        HIGH: '开',
                        LOW: '关'
                    }
                }
            }
        };
    }

    noop (){
        const msg = formatMessage({
            id: 'kblock.notify.nosupportonlinemode',
            defaultMessage: 'Not support in online mode'
        });
        return Promise.reject(msg)
    }

    servoSetup (args){
        const pin = firmataBoard.pin2firmata(args.PIN);
        if (pin >= 14){
            return Promise.reject(`No servo at ${args.PIN} for online mode`)
        }
        firmataBoard.servoConfig(pin, 400, 2600)
    }

    servoSetupGen (gen, block){
        gen.includes_['servo']  = '#include <Servo.h>';
        const pin = gen.valueToCode(block, 'PIN');
        gen.definitions_['servo_'+pin] = `Servo servo_${pin};`;
        gen.setupCodes_['servo_'+pin] = `servo_${pin}.attach(${pin}, 544, 2500)`;
    }

    servoWrite (args){
        const pin = firmataBoard.pin2firmata(args.PIN);
        let deg = parseInt(args.DEG, 10);
        firmataBoard.servoWrite(pin, deg);
    }

    servoWriteGen (gen, block){
        const pin = gen.valueToCode(block, 'PIN');
        const deg = gen.valueToCode(block, 'DEG');
        return `servo_${pin}.write(${deg})`;
    }

    geekservomap (args){
        const deg = args.DEG;
        return (deg-90)*20/3+1500;
    }

    geekservomapGen (gen, block){
        const deg = gen.valueToCode(block, 'DEG');
        gen.definitions_['geekservomap'] = `int geekServoMap(int degree){
  return (degree-90) * 20 / 3 + 1500;
}`;
        const code = `geekServoMap(${deg})`;
        return [code, 0];
    }

    geekservo2kgmap (args){
        let deg = args.DEG;
        deg = Math.round((deg-180)*50/9+1500);
        if (deg < 544) deg = 544;
        if (deg > 2500) deg = 2500;
        return deg;
    }

    geekservo2kgmapGen (gen, block){
        const deg = gen.valueToCode(block, 'DEG');
        gen.definitions_['geekservo2kgmap'] = `int geekServo2kgMap(int degree){
  return (degree-180) * 50 / 9 + 1500;
}`;
        const code = `geekServo2kgMap(${deg})`;
        return [code, 0];
    }
    
    buzzer (args){
        const pin = firmataBoard.pin2firmata(args.PIN);
        if (!this.bz){
            this.bz = new five.Piezo(pin);
        }
        this.bz.pin = pin;
        if (firmataBoard.pins[pin].mode !== 1){
            firmataBoard.pinMode(pin, 1)
        }
        this.bz.frequency(parseInt(args.FREQ), parseInt(args.DELAY));
    }

    buzzerGen (gen, block){
        return gen.template2code(block, 'tone');
    }
    
    music (args){
        const pin = firmataBoard.pin2firmata(args.PIN);
        if (!this.bz){
            this.bz = new five.Piezo(pin);
        }
        this.bz.pin = pin;
        if (firmataBoard.pins[pin].mode !== 1){
            firmataBoard.pinMode(pin, 1)
        }
        const notes = args.NOTES;
        const song = [];
        let note = 'C';
        let octave = '4';
        let clap = 0.128;
        for (let i=0;i<notes.length;i++){
            if(notes[i]>='a' && notes[i]<='g'){
                note = notes[i];
            }else if(notes[i]=='r'){
                note = null;
            }else if(notes[i]>='2' && notes[i]<='6'){
                octave = notes[i] - '0';
            }else if(notes[i]==':'){
                i++;
                clap = parseInt(notes[i])*0.125;
            }else if(notes[i]==' '){ // play until we meet a space
                song.push([note+octave, clap])
            }
        }
        console.log(song);
        this.bz.play({
            song,
            tempo: 40
        })
    }

    musicGen (gen, block){
        gen.definitions_['buzzMusic'] = `const int noteMap[] = {440, 494, 262, 294, 330, 349, 392};
void buzzMusic(int pin, const char * notes){
    int freq;
    int len = strlen(notes);
    int octave = 4;
    int duration = 500;
    for(int i=0;i < len;i++){
        if(notes[i]>='a' && notes[i]<='g'){
          freq = noteMap[notes[i]-'a'];
        }else if(notes[i]=='r'){
          freq = 0;
        }else if(notes[i]>='2' && notes[i]<='6'){
          octave = notes[i] - '0';
        }else if(notes[i]==':'){
          i++;
          duration = (notes[i] - '0')*125;
        }else if(notes[i]==' '){ // play until we meet a space
          freq *= pow(2, octave-4);
          tone(pin, freq, duration);
      delay(duration);
        }
    }
}`;

        return gen.template2code(block, 'buzzMusic');
    }
    
    relay(args){
        const pin = args.PIN;
        const value = args.ONOFF === 'HIGH' ? 1:0;
        firmataBoard.digitalWrite(firmataBoard.pin2firmata(pin), value ? 1 : 0);
    }

    relayGen (gen, block){
        const pin = gen.valueToCode(block, 'PIN');
        gen.setupCodes_['relay_'+pin] = `pinMode(${pin}, OUTPUT)`;
        return gen.template2code(block, 'digitalWrite');
    }
    
    motor (args){
        let speed = parseInt(args.SPEED);
        const dir = firmataBoard.pin2firmata(args.DIR);
        firmataBoard.pinMode(dir, firmataBoard.MODES.OUTPUT);
        const pwm = firmataBoard.pin2firmata(args.PWM);
        firmataBoard.pinMode(pwm, firmataBoard.MODES.PWM);
        if (speed>=0){
            firmataBoard.digitalWrite(dir, 0);
            firmataBoard.analogWrite(pwm, speed);
        } else {
            speed = -speed;
            firmataBoard.digitalWrite(dir, 1);
            firmataBoard.analogWrite(pwm, 255-speed);
        }
    }
    

    motorGen (gen, block){
        gen.definitions_['motorModule'] = `void motorModule(int pinDir, int pinPwm, int speed){
    pinMode(pinDir, OUTPUT);
    if(speed>=0){
        digitalWrite(pinDir, 1);
        speed = 255 - speed;
    }else{
        digitalWrite(pinDir, 0);
    }
    analogWrite(pinPwm, abs(speed));
}`;
        const pirDir = gen.valueToCode(block, 'DIR');
        const pinPwm = gen.valueToCode(block, 'PWM');
        const speed = gen.valueToCode(block, 'SPEED');

        return `motorModule(${pirDir}, ${pinPwm}, ${speed})`;
    }

    motorHGen (gen, block){
        gen.definitions_['motorBridge'] = `void motorBridge(int in1, int in2, int speed){
    if(speed>=0){
        analogWrite(in1, abs(speed));
        analogWrite(in2, 0);
    }else{
        analogWrite(in1, 0);
        analogWrite(in2, abs(speed));
    }
}`;
        const p1 = gen.valueToCode(block, 'IN1');
        const p2 = gen.valueToCode(block, 'IN2');
        const speed = gen.valueToCode(block, 'SPEED');
        return `motorBridge(${p1}, ${p2}, ${speed})`;
    }
}

module.exports = ActuatorExtension;
