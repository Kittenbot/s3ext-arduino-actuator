const formatMessage = Scratch.formatMessage

export const intlMap = {
  Actuator_geekservomap: formatMessage({ id: "Actuator_geekservomap", default: "GeekServo Map [DEG] ° "}),
  Actuator_geekservo2kgmap: formatMessage({ id: "Actuator_geekservo2kgmap", default: "GeekServo 2KG Map [DEG] ° "}),
  Actuator_servoSetup: formatMessage({ id: "Actuator_servoSetup", default: "null "}),
  Actuator_servoWrite: formatMessage({ id: "Actuator_servoWrite", default: "null "}),
  Actuator_buzzer: formatMessage({ id: "Actuator_buzzer", default: "null "}),
  Actuator_music: formatMessage({ id: "Actuator_music", default: "null "}),
  Actuator_relay: formatMessage({ id: "Actuator_relay", default: "null "}),
  Actuator_motorModule: formatMessage({ id: "Actuator_motorModule", default: "null "}),
  Actuator_motorH: formatMessage({ id: "Actuator_motorH", default: "null "}),
  Actuator_menu_onoff_HIGH: HIGH,
  Actuator_menu_onoff_LOW: LOW
}